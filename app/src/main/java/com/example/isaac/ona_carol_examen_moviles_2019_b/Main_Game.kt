package com.example.isaac.ona_carol_examen_moviles_2019_b

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main__game.*
import kotlin.math.abs
import android.widget.ArrayAdapter
import com.example.isaac.pilatuna_isaac_moviles_2018b.R


class Main_Game : AppCompatActivity() {
    internal  lateinit var puntaje_text_view: TextView;
    internal lateinit var  time_left_textView: TextView;
    internal lateinit var btn_iniciar: Button;
    internal lateinit var btn_presionar: Button;
    internal lateinit var list_view: ListView;
    internal var score =0
    internal var n=0
    internal var gameStarted = false;
    internal lateinit var countDownTimer: CountDownTimer
    internal var listaDePuntajes= ArrayList < String >();

    internal val countDownInterval=1000L
    internal val initialCountDown = 10000L
    internal var timeLeft = 10
    internal var timerCorriendo=false;

    internal var addScore=0

    internal var rondas=0

    companion object {
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        private val N_KEY = "N_KEY"
        private val RONDAS_KEY="RONDAS_KEY"
        private val PUNTAJES_KEY="PUNTAJES_KEY"
        private val TIMER_CORRIENDO_KEY="TIMER_CORRIENDO_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main__game)
        listaDePuntajes = ArrayList < String >()
        var listaAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaDePuntajes)
        puntaje_text_view = findViewById(R.id.puntaje_text_view)
        time_left_textView=findViewById(R.id.time_left_text_view)
        time_left_textView.setTextColor(Color.alpha(0))
        btn_iniciar=findViewById(R.id.btnIniciar)
        btn_presionar=findViewById(R.id.btnPresionar)
        list_view=findViewById(R.id.lista_Scores)
        list_view.adapter=listaAdapter
        if(savedInstanceState!=null){
            score=savedInstanceState.getInt(SCORE_KEY)
            timeLeft=savedInstanceState.getInt(TIME_LEFT_KEY)
            n=savedInstanceState.getInt(N_KEY)
            rondas=savedInstanceState.getInt(RONDAS_KEY)
            listaDePuntajes=savedInstanceState.getStringArrayList(PUNTAJES_KEY)
            timerCorriendo=savedInstanceState.getBoolean(TIMER_CORRIENDO_KEY)
            listaAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaDePuntajes)
            list_view.adapter=listaAdapter
            if(timerCorriendo){
                Toast.makeText(this,"Ronda Reiniciada",Toast.LENGTH_LONG).show()
            }
            resetGame()
        }else{
            resetGame()
        }
        btn_iniciar.setOnClickListener{_ -> startGame()}
        btn_presionar.setOnClickListener{_ -> endGame(listaAdapter)}
    }



    private fun resetGame(){

        timeLeft=10
        n=0
        addScore=0
        val gameScore = getString(R.string.puntaje, Integer.toString(score))
        puntaje_text_view.text=gameScore

        val nText=getString(R.string.n,Integer.toString(n))
        n_text_view.text=nText

        btn_iniciar.isClickable=true
        btn_iniciar.setBackgroundColor(Color.rgb(100,200,150))
        btn_iniciar.setTextColor(Color.BLACK)

        btn_presionar.isClickable=false
        btn_presionar.setBackgroundColor(Color.WHITE)
        btn_presionar.setTextColor(Color.LTGRAY)


        val timeLeftText = getString(R.string.tiempoRestante, Integer.toString(timeLeft))
        time_left_textView.text = timeLeftText
        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timerCorriendo=true
                timeLeft = millisUntilFinished.toInt() /1000
                val timeShown=10-timeLeft
                time_left_textView.text= getString(R.string.tiempoRestante, Integer.toString(timeShown))
            }

            override fun onFinish() {
                timerCorriendo=false
                endGameNoScore()
            }
        }
        gameStarted=false
    }

    private fun startGame(){
        btn_iniciar.isClickable=false
        btn_iniciar.setBackgroundColor(Color.WHITE)
        btn_iniciar.setTextColor(Color.LTGRAY)

        btn_presionar.isClickable=true
        btn_presionar.setBackgroundColor(Color.rgb(100,200,150))
        btn_presionar.setTextColor(Color.BLACK)
        countDownTimer.start()
        gameStarted=true
        n=(0..10).shuffled().first()
        n_text_view.text=getString(R.string.n,Integer.toString(n))
    }


    private fun endGame(listaAdapter: ArrayAdapter<String>) {
        var diferencia:Int=abs((10-timeLeft.toInt())-n.toInt())
        if(diferencia==0){
            addScore=100
        }else{
            if(diferencia==1){
                addScore=50

            }else{
                addScore=0
            }
        }
        rondas++
        score+=addScore
        val puntajeRonda=getString(R.string.puntaje_rondas,Integer.toString(rondas),Integer.toString(score),Integer.toString(addScore))
        listaDePuntajes.add(puntajeRonda.toString())
        listaAdapter.notifyDataSetChanged()


        Toast.makeText(this,getString(R.string.puntaje_obtenido, Integer.toString(addScore)),Toast.LENGTH_LONG).show()
        timerCorriendo=false
        countDownTimer.cancel()
        resetGame()

    }

    private fun endGameNoScore(){
        Toast.makeText(this,getString(R.string.game_over_message_no_score),Toast.LENGTH_LONG).show()
        resetGame()
    }


    fun restoreGame(){
        resetGame()





    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(SCORE_KEY,score)
        outState.putInt(TIME_LEFT_KEY,timeLeft)
        outState.putInt(N_KEY,n)
        outState.putInt(RONDAS_KEY,rondas)
        outState.putStringArrayList(PUNTAJES_KEY,listaDePuntajes)
        outState.putBoolean(TIMER_CORRIENDO_KEY, timerCorriendo)
        countDownTimer.cancel()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
